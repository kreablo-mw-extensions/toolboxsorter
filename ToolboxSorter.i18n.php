<?php
/**
 * Internationalisation file for extension ToolboxSorter
 *
 * @file
 * @ingroup Extensions
 */

$messages = array();

$messages['en'] = array(
    'toolboxsorter' => 'Toolbox Sorter',
    'toolboxsorter-desc' => 'Sort and manipulate the items in the toolbox',
);

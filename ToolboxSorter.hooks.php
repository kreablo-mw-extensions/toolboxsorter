<?php
/**
 * Copyright (C) 2013 Andreas Jonsson <andreas.jonsson@kreablo.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Extensions
 */

if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

class ToolboxSorterHooks {

    public static function onBaseTemplateToolbox( $skin, &$toolbox ) {
        global $weToolboxSorterItems;

        if ( is_array($weToolboxSorterItems) ) {

            foreach ( $toolbox as $key => $value ) {
                if ( isset($weToolboxSorterItems[$key]) && $weToolboxSorterItems[$key] === false ) {
                    unset($toolbox[$key]);
                }
            }

            foreach ( $weToolboxSorterItems as $key => $value ) {
                if ( is_array($weToolboxSorterItems[$key]) ) {
                    $toolbox[$key] = self::handleToolboxItem($weToolboxSorterItems[$key]);
                }
            }

            uksort( $toolbox, 'ToolboxSorterHooks::cmp' );
        }

        return true;
    }

    private static function value( $a ) {
        global $weToolboxSorterItems;
        if ( isset($weToolboxSorterItems[$a]) ) {
            $i = $weToolboxSorterItems[$a];
            if (is_array($i) && isset($i['sort-order'])) {
                return $i['sort-order'];
            }
            if (is_numeric($i)) {
                return $i;
            }
        }
        return 10000000;
    }

    public static function cmp( $a, $b ) {
        return self::value( $a ) - self::value( $b );
    }

    private static function handleToolboxItem( $item ) {
		global $wgOut;
        if ( !isset($item['href']) ) {
            if ( isset($item['href-title']) ) {
                $title = Title::newFromText( $item['href-title'] );
                $item['href'] = $title->getLocalURL();
            } elseif ( isset($item['href-title-msg']) ) {
                $title = Title::newFromText( $wgOut->msg($item['href-title-msg'])->text() );
                $item['href'] = $title->getLocalURL();
            }
        }
        return $item;
    }
}